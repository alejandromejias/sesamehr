<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240318173617 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE users (id UUID NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX user_index_id ON users (id)');
        $this->addSql('COMMENT ON COLUMN users.created_at IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN users.updated_at IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN users.deleted_at IS \'(DC2Type:datetime)\'');
        $this->addSql('CREATE TABLE work_entries (id UUID NOT NULL, user_id UUID NOT NULL, start_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, end_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX work_entry_index_id ON work_entries (id)');
        $this->addSql('COMMENT ON COLUMN work_entries.start_date IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN work_entries.end_date IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN work_entries.created_at IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN work_entries.updated_at IS \'(DC2Type:datetime)\'');
        $this->addSql('COMMENT ON COLUMN work_entries.deleted_at IS \'(DC2Type:datetime)\'');
        $this->addSql('ALTER TABLE work_entries ADD CONSTRAINT work_entries_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE users');
        $this->addSql('DROP TABLE work_entries');
    }
}
