# Sesame HR API

Welcome to the Sesame HR REST API repository, a human resource management application designed to streamline and optimize personnel administration processes in organizations. This README provides detailed instructions on how to set up and use the API.

## API Documentation

The API documentation is defined using the OpenAPI specification. You can find the OpenAPI specification file at the following link:

[OpenAPI Specification](./openapi.yml)

This file contains all the necessary definitions to understand and interact with the API, including available endpoints, parameters, data schemas, and responses.

It is recommended to use tools like [GitLab](https://gitlab.com/alejandromejias/sesamehr/-/blob/main/openapi.yml) for a more user-friendly way to visualize and interact with the API documentation.

## Prerequisites

Before setting the Sesame HR, ensure the following are installed on your system:

- [Docker](https://www.docker.com/get-started/)
- [Git](https://git-scm.com/)

## Setup

Follow these steps to set the Sesame HR on your system:

1. **Clone the Repository**:
   ```bash
   git clone https://gitlab.com/alejandromejias/sesamehr.git
   cd sesamehr
   ```
2. **Run Setup Script**:

   This will set up the environment, build the Docker containers, and create the database. The app is ready to be used. No additional action is required
    ```bash
    make install
    ```

## Tests

Sesame HR has unit and integration tests to ensure the quality and reliability of the application.

- **Unit Tests**:

    ```bash
    make test-unit
    ```

- **Integration Tests**:

    ```bash
    make test-integration
    ```

- **All Tests**:

  To run both unit and integration tests, use:

    ```bash
    make test
    ```

## Development Tools

SesameHR uses the following development tools:

- **Build**: Set up and build the Docker containers.
  
    ```bash
    make build
    ```

- **Up**: Start the Docker containers.

    ```bash
    make up
    ```

- **Down**: Tear down the Docker containers and prune volumes.

    ```bash
    make down
    ```

- **Lint-Scan**: Run code linting scan.

    ```bash
    make lint-scan
    ```

- **Lint-Fix**: Automatically fix linting issues.

    ```bash
    make lint-fix
    ```
