install:
	cp -i .env.example .env
	make build
	docker compose run --rm symfony php bin/console doctrine:database:create
	docker compose run --rm symfony php bin/console doctrine:schema:create

build:
	docker compose up -d --build
	docker compose run --rm symfony composer install

up:
	docker compose up -d

down:
	docker compose down
	docker volume prune

test-unit:
	docker compose run --rm symfony composer test:unit

test-integration:
	docker compose run --rm symfony composer test:integration

test:
	docker compose run --rm symfony composer test

lint-scan:
	docker compose run --rm symfony composer lint:scan

lint-fix:
	docker compose run --rm symfony composer lint:fix
