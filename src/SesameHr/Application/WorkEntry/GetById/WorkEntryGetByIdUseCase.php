<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\GetById;

use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Exception\NotFoundException;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryGetByIdUseCase
{
    public function __construct(
        private WorkEntryRepository $workEntryRepository
    ) {
    }

    public function __invoke(WorkEntryGetByIdRequest $request): WorkEntryGetByIdResponse
    {
        $user = $this->workEntryRepository->ofId(Uuid::from($request->id()));
        $this->checkWorkEntryIsFound($user);
        return new WorkEntryGetByIdResponse($user);
    }

    private function checkWorkEntryIsFound(?WorkEntry $workEntry): void
    {
        if (!is_null($workEntry) && !$workEntry->isDeleted()) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('id')]);
        throw NotFoundException::fromErrors(errors: $errors);
    }
}
