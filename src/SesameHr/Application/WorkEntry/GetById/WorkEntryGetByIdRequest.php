<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\GetById;

final readonly class WorkEntryGetByIdRequest
{
    public function __construct(
        private string $id
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }
}
