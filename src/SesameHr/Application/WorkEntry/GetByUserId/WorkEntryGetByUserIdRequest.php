<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\GetByUserId;

final readonly class WorkEntryGetByUserIdRequest
{
    public function __construct(
        private string $userId
    ) {
    }

    public function userId(): string
    {
        return $this->userId;
    }
}
