<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\GetByUserId;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\Filter;
use App\SesameHr\Shared\Domain\Criteria\Filters;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryGetByUserIdUseCase
{
    public function __construct(
        private UserRepository $userRepository,
        private WorkEntryRepository $workEntryRepository
    ) {
    }

    public function __invoke(WorkEntryGetByUserIdRequest $request): WorkEntryGetByUserIdResponse
    {
        $user = $this->userRepository->ofId(new Uuid($request->userId()));
        $this->checkUserIsFound($user);
        $workEntries = $this->workEntryRepository->search($this->createCriteria($user));
        return new WorkEntryGetByUserIdResponse($workEntries);
    }

    private function checkUserIsFound(?User $user): void
    {
        if (!is_null($user)) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('id')]);
        throw BadRequestException::fromErrors(errors: $errors);
    }

    private function createCriteria(User $user): Criteria
    {
        $filterUserId = Filter::fromEqual('userId', (string)$user->id());
        $filterDeletedAt = Filter::fromEqual('deletedAt', null);
        return Criteria::from(
            Filters::of([$filterUserId, $filterDeletedAt])
        );
    }
}
