<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\GetByUserId;

use App\SesameHr\Domain\WorkEntry\WorkEntries;
use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Shared\Application\Response;

final readonly class WorkEntryGetByUserIdResponse implements Response
{
    public function __construct(
        private WorkEntries $workEntries
    ) {
    }

    public function data(): array
    {
        return $this->workEntries->map(function (WorkEntry $workEntry) {
            return $workEntry->summary();
        });
    }
}
