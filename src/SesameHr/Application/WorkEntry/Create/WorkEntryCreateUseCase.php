<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Create;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryCreateUseCase
{
    public function __construct(
        private UserRepository $userRepository,
        private WorkEntryRepository $workEntryRepository,
        private EventBus $eventBus
    ) {
    }

    public function __invoke(WorkEntryCreateRequest $request): WorkEntryCreateResponse
    {
        $user = $this->userRepository->ofId(new Uuid($request->userId()));
        $this->checkUserIsFound($user);
        $newWorkEntry = WorkEntry::create($request->userId(), $request->startDate(), $request->endDate());
        $this->workEntryRepository->save($newWorkEntry);
        $this->eventBus->publish($newWorkEntry->pullEvents());
        return new WorkEntryCreateResponse($newWorkEntry);
    }

    private function checkUserIsFound(?User $user): void
    {
        if (!is_null($user)) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('userId')]);
        throw BadRequestException::fromErrors(errors: $errors);
    }
}
