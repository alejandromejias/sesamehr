<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Create;

final readonly class WorkEntryCreateRequest
{
    public function __construct(
        private string $userId,
        private string $startDate,
        private ?string $endDate = null
    ) {
    }

    public function userId(): string
    {
        return $this->userId;
    }

    public function startDate(): string
    {
        return $this->startDate;
    }

    public function endDate(): ?string
    {
        return $this->endDate;
    }
}
