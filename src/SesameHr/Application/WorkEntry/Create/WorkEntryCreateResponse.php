<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Create;

use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Shared\Application\Response;

final readonly class WorkEntryCreateResponse implements Response
{
    public function __construct(
        private WorkEntry $workEntry
    ) {
    }

    public function data(): array
    {
        return $this->workEntry->summary();
    }
}
