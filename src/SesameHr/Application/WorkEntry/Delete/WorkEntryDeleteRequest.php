<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Delete;

final class WorkEntryDeleteRequest
{
    public function __construct(
        private string $id
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }
}
