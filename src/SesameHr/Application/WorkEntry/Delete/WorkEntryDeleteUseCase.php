<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Delete;

use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryDeleteUseCase
{
    public function __construct(
        private WorkEntryRepository $workEntryRepository,
        private EventBus $eventBus
    ) {
    }

    public function __invoke(WorkEntryDeleteRequest $request): void
    {
        $workEntry = $this->workEntryRepository->ofId(Uuid::from($request->id()));
        $this->checkWorkEntryIsFound($workEntry);
        $workEntry->delete();
        $this->workEntryRepository->save($workEntry);
        $this->eventBus->publish($workEntry->pullEvents());
    }

    private function checkWorkEntryIsFound(?WorkEntry $workEntry): void
    {
        if (!is_null($workEntry)) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('id')]);
        throw BadRequestException::fromErrors(errors: $errors);
    }
}
