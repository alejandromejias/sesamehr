<?php

declare(strict_types=1);

namespace App\SesameHr\Application\WorkEntry\Update;

final readonly class WorkEntryUpdateRequest
{
    public function __construct(
        private string $id,
        private string $startDate,
        private ?string $endDate = null
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }

    public function startDate(): string
    {
        return $this->startDate;
    }

    public function endDate(): ?string
    {
        return $this->endDate;
    }
}
