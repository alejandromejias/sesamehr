<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\GetAll;

use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\Filter;
use App\SesameHr\Shared\Domain\Criteria\Filters;

final readonly class UserGetAllUseCase
{
    public function __construct(
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(): UserGetAllResponse
    {
        $users = $this->userRepository->search($this->createCriteria());
        return new UserGetAllResponse($users);
    }

    private function createCriteria(): Criteria
    {
        $filter = Filter::fromEqual('deletedAt', null);
        return Criteria::from(
            Filters::of([$filter])
        );
    }
}
