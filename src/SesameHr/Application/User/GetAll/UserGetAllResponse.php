<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\GetAll;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\Users;
use App\SesameHr\Shared\Application\Response;

final readonly class UserGetAllResponse implements Response
{
    public function __construct(
        private Users $users
    ) {
    }

    public function data(): array
    {
        return $this->users->map(function (User $user) {
            return $user->summary();
        });
    }
}
