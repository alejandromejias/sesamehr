<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Create;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\User\Users;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\Filter;
use App\SesameHr\Shared\Domain\Criteria\Filters;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;

final readonly class UserCreateUseCase
{
    public function __construct(
        private UserRepository $userRepository,
        private EventBus $eventBus
    ) {
    }

    public function __invoke(UserCreateRequest $request): UserCreateResponse
    {
        $users = $this->userRepository->search($this->createCriteria($request));
        $this->checkUserIsNotFound($users);
        $newUser = User::create($request->name(), $request->email());
        $this->userRepository->save($newUser);
        $this->eventBus->publish($newUser->pullEvents());
        return new UserCreateResponse($newUser);
    }

    private function createCriteria(UserCreateRequest $request): Criteria
    {
        $filter = Filter::fromEqual('email', $request->email());
        return Criteria::from(
            Filters::of([$filter])
        );
    }

    private function checkUserIsNotFound(Users $users): void
    {
        if ($users->isEmpty()) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromExistingValue('email')]);
        throw BadRequestException::fromErrors(errors: $errors);
    }
}
