<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Create;

final readonly class UserCreateRequest
{
    public function __construct(
        private string $name,
        private string $email
    ) {
    }

    public function name(): string
    {
        return $this->name;
    }

    public function email(): string
    {
        return $this->email;
    }
}
