<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Create;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Shared\Application\Response;

final readonly class UserCreateResponse implements Response
{
    public function __construct(
        private User $user
    ) {
    }

    public function data(): array
    {
        return $this->user->summary();
    }
}
