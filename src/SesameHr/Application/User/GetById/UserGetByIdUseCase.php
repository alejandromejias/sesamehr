<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\GetById;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Exception\NotFoundException;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class UserGetByIdUseCase
{
    public function __construct(
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(UserGetByIdRequest $request): UserGetByIdResponse
    {
        $user = $this->userRepository->ofId(Uuid::from($request->id()));
        $this->checkUserIsFound($user);
        return new UserGetByIdResponse($user);
    }

    private function checkUserIsFound(?User $user): void
    {
        if (!is_null($user) && !$user->isDeleted()) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('id')]);
        throw NotFoundException::fromErrors(errors: $errors);
    }
}
