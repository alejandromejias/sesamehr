<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\GetById;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Shared\Application\Response;

final readonly class UserGetByIdResponse implements Response
{
    public function __construct(
        private User $user
    ) {
    }

    public function data(): array
    {
        return $this->user->summary();
    }
}
