<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Update;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class UserUpdateUseCase
{
    public function __construct(
        private UserRepository $userRepository,
        private EventBus $eventBus
    ) {
    }

    public function __invoke(UserUpdateRequest $request): UserUpdateResponse
    {
        $user = $this->userRepository->ofId(Uuid::from($request->id()));
        $this->checkUserIsFound($user);
        $user->update($request->name(), $request->email());
        $this->userRepository->save($user);
        $this->eventBus->publish($user->pullEvents());
        return new UserUpdateResponse($user);
    }

    private function checkUserIsFound(?User $user): void
    {
        if (!is_null($user)) {
            return;
        }
        $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('id')]);
        throw BadRequestException::fromErrors(errors: $errors);
    }
}
