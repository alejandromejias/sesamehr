<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Update;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Shared\Application\Response;

final readonly class UserUpdateResponse implements Response
{
    public function __construct(
        private User $user
    ) {
    }

    public function data(): array
    {
        return $this->user->summary();
    }
}
