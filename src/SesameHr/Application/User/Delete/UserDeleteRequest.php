<?php

declare(strict_types=1);

namespace App\SesameHr\Application\User\Delete;

final readonly class UserDeleteRequest
{
    public function __construct(
        private string $id
    ) {
    }

    public function id(): string
    {
        return $this->id;
    }
}
