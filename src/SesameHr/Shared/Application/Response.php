<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Application;

interface Response
{
    public function data(): array;
}
