<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Exception;

use App\SesameHr\Shared\Domain\Exception\ExceptionBase;
use App\SesameHr\Shared\Domain\Exception\MethodNotAllowedException;
use App\SesameHr\Shared\Domain\Exception\NotFoundException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException as SymfonyMethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException as SymfonyNotFoundHttpException;
use Throwable;

class ExceptionHandler
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }
    private const UNDEFINED_ROUTE = 'UNDEFINED_ROUTE';

    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $this->mapException($event->getThrowable());
        $responseData = $this->responseData($event, $exception);
        $response = new JsonResponse(
            $responseData,
            $this->responseCode($exception)
        );
        $event->setResponse($response);
        $this->logger->error(
            sprintf('Unhandled Exception: %s', $exception->getMessage()),
            $responseData
        );
    }

    private function mapException(Throwable $exception): Throwable
    {
        return match (true) {
            $exception instanceof SymfonyNotFoundHttpException => NotFoundException::fromDefault(),
            $exception instanceof SymfonyMethodNotAllowedHttpException => MethodNotAllowedException::fromDefault(),
            default => $exception
        };
    }

    private function responseData(ExceptionEvent $event, Throwable $exception): array
    {
        return [
            'tag' => $event->getRequest()->attributes->get('_route') ?? self::UNDEFINED_ROUTE,
            'message' => $exception->getMessage(),
            'details' => $exception instanceof ExceptionBase ? $exception->errors()->toArray() : []
        ];
    }

    private function responseCode(Throwable $exception): int
    {
        return !empty($exception->getCode()) ? $exception->getCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
    }
}
