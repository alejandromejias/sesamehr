<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Event;

use App\SesameHr\Shared\Domain\Event\DomainEvents;
use App\SesameHr\Shared\Domain\Event\EventBus;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

final readonly class InMemoryEventBus implements EventBus
{
    public function __construct(
        private MessageBusInterface $bus,
        private LoggerInterface $logger
    ) {
    }

    public function publish(DomainEvents $events): void
    {
        foreach ($events as $event) {
            try {
                $this->bus->dispatch($event);
            } catch (Throwable $exception) {
                $this->logger->critical(
                    sprintf(
                        'Domain Event with id %s cannot be publish: %s',
                        $event->eventId(),
                        $exception->getMessage()
                    ),
                    $exception->getTrace()
                );
            }
        }
    }
}
