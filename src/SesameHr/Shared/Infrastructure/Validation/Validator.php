<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Validation;

use Symfony\Component\HttpFoundation\Request;

abstract class Validator
{
    public function __construct(
        protected ValidationContext $context
    ) {
    }

    abstract public function validate(Request $request): void;
}
