<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Validation;

use App\SesameHr\Shared\Domain\Exception\BadRequestException;

class ValidationContext
{
    public function __construct(
        private ValidationRules $rules,
        private ValidationExceptionErrorHandler $handler
    ) {
    }

    public function addRule(ValidationRule $rule): self
    {
        $this->rules->add($rule);
        return $this;
    }

    public function validate(): void
    {
        foreach ($this->rules as $rule) {
            $this->handleRuleErrors($rule);
        }
        $this->checkErrors();
    }

    private function handleRuleErrors(ValidationRule $rule): void
    {
        foreach ($rule->errors() as $error) {
            $this->handler->handleError($error);
        }
    }

    private function checkErrors(): void
    {
        if (!$this->handler()->hasErrors()) {
            return;
        }
        throw BadRequestException::fromErrors($this->handler()->errors());
    }

    public function handler(): ValidationExceptionErrorHandler
    {
        return $this->handler;
    }
}
