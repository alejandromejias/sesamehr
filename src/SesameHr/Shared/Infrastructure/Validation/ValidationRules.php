<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Validation;

use App\SesameHr\Shared\Domain\Collection;

class ValidationRules extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof ValidationRule;
    }
}
