<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Validation;

use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;

abstract class ValidationRule
{
    protected ExceptionErrors $errors;

    public function __construct(mixed $value, string $target)
    {
        $this->errors = new ExceptionErrors();
        $this->checkRule($value, $target);
    }

    abstract protected function checkRule(mixed $value, string $target): void;

    protected function addError(ExceptionError $error): void
    {
        $this->errors->add($error);
    }

    public function errors(): ExceptionErrors
    {
        return $this->errors;
    }
}
