<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Validation;

use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;

final class ValidationExceptionErrorHandler
{
    private ExceptionErrors $errors;

    public function __construct()
    {
        $this->errors = new ExceptionErrors();
    }

    public function handleError(ExceptionError $error): void
    {
        $this->errors->add($error);
    }

    public function errors(): ExceptionErrors
    {
        return $this->errors;
    }

    public function hasErrors(): bool
    {
        return !$this->errors->isEmpty();
    }
}
