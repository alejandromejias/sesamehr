<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Doctrine;

use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\FilterOperator;
use Doctrine\Common\Collections\Criteria as DoctrineCriteria;
use Doctrine\Common\Collections\Expr\Comparison;
use Doctrine\Common\Collections\Expr\Value;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

abstract class DoctrineRepository
{
    protected final const FILTER_OPERATOR_MAP = [
        FilterOperator::EQUAL->value => Comparison::EQ
    ];


    public function __construct(
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    protected function persist(mixed $entity): void
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    }

    protected function repository(string $entityClass): EntityRepository
    {
        return $this->entityManager->getRepository($entityClass);
    }

    protected function createDoctrineCriteria(Criteria $criteria): DoctrineCriteria
    {
        $doctrineCriteria = DoctrineCriteria::create();
        foreach ($criteria->filters() as $filter) {
            $expression = new Comparison(
                $filter->field(),
                self::FILTER_OPERATOR_MAP[$filter->operator()->value],
                new Value($filter->value())
            );
            $doctrineCriteria->andWhere($expression);
        }
        return $doctrineCriteria;
    }
}
