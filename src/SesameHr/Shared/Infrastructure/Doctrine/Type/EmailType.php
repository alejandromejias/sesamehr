<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Doctrine\Type;

use App\SesameHr\Shared\Domain\Email;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;

// phpcs:disable SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingAnyTypeHint
class EmailType extends StringType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? new Email($value) : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? $value : null;
    }
}
