<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Doctrine\Type;

use App\SesameHr\Shared\Domain\Uuid;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\GuidType;

// phpcs:disable SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingAnyTypeHint
class UuidType extends GuidType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? new Uuid($value) : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? $value : null;
    }
}
