<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Infrastructure\Doctrine\Type;

use App\SesameHr\Shared\Domain\DateTime;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\DateTimeImmutableType;

// phpcs:disable SlevomatCodingStandard.TypeHints.ParameterTypeHint.MissingAnyTypeHint
class DateTimeType extends DateTimeImmutableType
{
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? new DateTime($value) : null;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return !is_null($value) ? $value : null;
    }

    public function getName(): string
    {
        return 'datetime';
    }
}
