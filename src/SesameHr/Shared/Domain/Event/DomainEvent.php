<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Event;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Uuid;

abstract readonly class DomainEvent
{
    public function __construct(
        private Uuid $aggregateId,
        private Uuid $eventId,
        private DateTime $occurredOn
    ) {
    }

    abstract public static function eventName(): string;

    public function aggregatedId(): Uuid
    {
        return $this->aggregateId;
    }

    public function eventId(): Uuid
    {
        return $this->eventId;
    }

    public function occurredOn(): DateTime
    {
        return $this->occurredOn;
    }
}
