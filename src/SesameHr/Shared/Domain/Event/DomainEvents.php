<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Event;

use App\SesameHr\Shared\Domain\Collection;

final class DomainEvents extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof DomainEvent;
    }
}
