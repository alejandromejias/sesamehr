<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Event;

interface EventBus
{
    public function publish(DomainEvents $events): void;
}
