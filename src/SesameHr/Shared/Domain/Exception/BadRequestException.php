<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use Throwable;

final class BadRequestException extends ExceptionBase
{
    private const BAD_REQUEST_MESSAGE
        = 'The request cannot be processed due to invalid syntax, parameters or inconsistent data.';
    private const BAD_REQUEST_CODE = 400;

    public function __construct(
        string $message = self::BAD_REQUEST_MESSAGE,
        ?Throwable $previous = null,
        ExceptionErrors $errors = new ExceptionErrors()
    ) {
        parent::__construct(
            $message,
            self::BAD_REQUEST_CODE,
            $previous,
            $errors
        );
    }
}
