<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use Throwable;

class NotFoundException extends ExceptionBase
{
    private const NOT_FOUND_MESSAGE
        = 'The resource you are trying to get is not found.';
    private const NOT_FOUND_CODE = 404;

    public function __construct(
        string $message = self::NOT_FOUND_MESSAGE,
        ?Throwable $previous = null,
        ExceptionErrors $errors = new ExceptionErrors()
    ) {
        parent::__construct(
            $message,
            self::NOT_FOUND_CODE,
            $previous,
            $errors
        );
    }
}
