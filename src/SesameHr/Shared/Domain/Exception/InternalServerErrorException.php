<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use Throwable;

final class InternalServerErrorException extends ExceptionBase
{
    private const INTERNAL_SERVER_ERROR_MESSAGE = 'An unexpected error occurred. Contact with server administrator';
    private const INTERNAL_SERVER_ERROR_CODE = 500;

    public function __construct(
        string $message = self::INTERNAL_SERVER_ERROR_MESSAGE,
        ?Throwable $previous = null,
        ExceptionErrors $errors = new ExceptionErrors()
    ) {
        parent::__construct(
            $message,
            self::INTERNAL_SERVER_ERROR_CODE,
            $previous,
            $errors
        );
    }
}
