<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

enum ExceptionErrorMessage: string
{
    case NULL_VALUE_ERROR_MESSAGE = 'This value is required.';
    case INVALID_VALUE_ERROR_MESSAGE = 'This value is invalid.';
    case EXISTING_VALUE_ERROR_MESSAGE = 'This value exists and is already in use.';
    case CANNOT_UPDATE_DELETED_RESOURCE_ERROR_MESSAGE = 'Cannot update deleted resource.';
    case CANNOT_DELETE_DELETED_RESOURCE_ERROR_MESSAGE = 'Cannot delete deleted resource.';
    case INCONSISTENT_DATA_ERROR_MESSAGE = 'Inconsistent data.';
}
