<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use Throwable;

final class MethodNotAllowedException extends ExceptionBase
{
    private const METHOD_NOT_ALLOWED_MESSAGE
        = 'The HTTP method specified is not allowed for the requested action or resource.';
    private const METHOD_NOT_ALLOWED_CODE = 405;

    public function __construct(
        string $message = self::METHOD_NOT_ALLOWED_MESSAGE,
        ?Throwable $previous = null,
        ExceptionErrors $errors = new ExceptionErrors()
    ) {
        parent::__construct(
            $message,
            self::METHOD_NOT_ALLOWED_CODE,
            $previous,
            $errors
        );
    }
}
