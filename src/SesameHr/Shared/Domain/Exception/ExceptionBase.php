<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use Exception;
use Throwable;

abstract class ExceptionBase extends Exception
{
    public function __construct(
        string $message = "",
        int $code = 0,
        ?Throwable $previous = null,
        private readonly ExceptionErrors $errors = new ExceptionErrors()
    ) {
        parent::__construct($message, $code, $previous);
    }

    public static function fromErrors(
        ExceptionErrors $errors
    ): static {
        return new static(errors: $errors);
    }

    public static function fromDefault(): static
    {
        return new static();
    }

    public function errors(): ExceptionErrors
    {
        return $this->errors;
    }
}
