<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

use App\SesameHr\Shared\Domain\Collection;

final class ExceptionErrors extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof ExceptionError;
    }
}
