<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Exception;

class ExceptionError
{
    public function __construct(
        private ExceptionErrorCode $code,
        private string $target,
        private ExceptionErrorMessage $message
    ) {
    }

    public static function fromNullValue(string $target): self
    {
        return self::from(
            ExceptionErrorCode::NULL_VALUE_ERROR_CODE,
            $target,
            ExceptionErrorMessage::NULL_VALUE_ERROR_MESSAGE
        );
    }

    public static function fromInvalidValue(string $target): self
    {
        return self::from(
            ExceptionErrorCode::INVALID_VALUE_ERROR_CODE,
            $target,
            ExceptionErrorMessage::INVALID_VALUE_ERROR_MESSAGE
        );
    }

    public static function fromExistingValue(string $target): self
    {
        return self::from(
            ExceptionErrorCode::EXISTING_VALUE_ERROR_CODE,
            $target,
            ExceptionErrorMessage::EXISTING_VALUE_ERROR_MESSAGE
        );
    }

    public static function fromCannotUpdateDeletedResource(string $target): self
    {
        return self::from(
            ExceptionErrorCode::CANNOT_UPDATE_DELETED_RESOURCE_ERROR_CODE,
            $target,
            ExceptionErrorMessage::CANNOT_UPDATE_DELETED_RESOURCE_ERROR_MESSAGE
        );
    }

    public static function fromCannotDeleteDeletedResource(string $target): self
    {
        return self::from(
            ExceptionErrorCode::CANNOT_DELETE_DELETED_RESOURCE_ERROR_CODE,
            $target,
            ExceptionErrorMessage::CANNOT_DELETE_DELETED_RESOURCE_ERROR_MESSAGE
        );
    }

    public static function fromInconsistentData(string $target): self
    {
        return self::from(
            ExceptionErrorCode::INCONSISTENT_DATA_ERROR_CODE,
            $target,
            ExceptionErrorMessage::INCONSISTENT_DATA_ERROR_MESSAGE
        );
    }

    public static function from(
        ExceptionErrorCode $code,
        string $target,
        ExceptionErrorMessage $message
    ): self {
        return new self(
            $code,
            $target,
            $message
        );
    }

    public function toArray(): array
    {
        return [
            'code' => $this->code()->value,
            'target' => $this->target(),
            'message' => $this->message()->value
        ];
    }

    public function code(): ExceptionErrorCode
    {
        return $this->code;
    }

    public function target(): string
    {
        return $this->target;
    }

    public function message(): ExceptionErrorMessage
    {
        return $this->message;
    }
}
