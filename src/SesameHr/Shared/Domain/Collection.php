<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain;

use App\SesameHr\Shared\Domain\Exception\InternalServerErrorException;
use ArrayIterator;
use Countable;
use IteratorAggregate;

abstract class Collection implements Countable, IteratorAggregate
{
    protected array $items;

    public function __construct(array $items = [])
    {
        $this->checkAreValidTypes($items);
        $this->items = $items;
    }

    private function checkAreValidTypes(array $items): void
    {
        foreach ($items as $item) {
            $this->checkIsValidType($item);
        }
    }


    public function add(mixed $item): void
    {
        $this->checkIsValidType($item);
        $this->items[] = $item;
    }

    private function checkIsValidType(mixed $item): void
    {
        if (!$this->isInstanceOf($item)) {
            throw new InternalServerErrorException(errors: ['Invalid item type collection']);
        }
    }

    public static function of(array $items = []): static
    {
        return new static($items);
    }

    abstract protected function isInstanceOf(mixed $item): bool;

    public function count(): int
    {
        return count($this->items);
    }

    public function isEmpty(): bool
    {
        return empty($this->items);
    }

    public function map(callable $callback): array
    {
        return (array_map($callback, $this->items));
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }

    public function toArray(): array
    {
        $values = [];
        foreach ($this->items as $item) {
            $values[] = method_exists($item, 'toArray')
                ? $item->toArray()
                : $this->toString($item);
        }
        return $values;
    }

    private function toString(mixed $item): string
    {
        return method_exists($item, '__toString')
            ? $item->__toString()
            : $item->toString();
    }
}
