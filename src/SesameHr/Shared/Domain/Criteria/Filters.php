<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Criteria;

use App\SesameHr\Shared\Domain\Collection;

final class Filters extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof Filter;
    }
}
