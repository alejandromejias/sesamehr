<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Criteria;

enum FilterOperator: string
{
    case EQUAL = 'EQUAL';
}
