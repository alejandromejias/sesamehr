<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Criteria;

final readonly class Filter
{
    public function __construct(
        private string $field,
        private FilterOperator $operator,
        private mixed $value
    ) {
    }

    public static function fromEqual(string $field, mixed $value): self
    {
        return self::from($field, FilterOperator::EQUAL, $value);
    }

    public static function from(
        string $field,
        FilterOperator $operator,
        mixed $value
    ): self {
        return new self($field, $operator, $value);
    }

    public function field(): string
    {
        return $this->field;
    }

    public function operator(): FilterOperator
    {
        return $this->operator;
    }

    public function value(): mixed
    {
        return $this->value;
    }
}
