<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Criteria;

final class Criteria
{
    public function __construct(
        private Filters $filters
    ) {
    }

    public static function from(Filters $filters): Criteria
    {
        return new self($filters);
    }

    /**
     * @return Filters<Filter>
     */
    public function filters(): Filters
    {
        return $this->filters;
    }
}
