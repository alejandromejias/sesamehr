<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain\Aggregate;

use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Event\DomainEvents;

abstract class AggregateRoot
{
    protected DomainEvents $events;

    public function __construct()
    {
        $this->resetEvents();
    }


    final public function pullEvents(): DomainEvents
    {
        $pulledEvents = $this->events;
        $this->resetEvents();
        return $pulledEvents;
    }

    final protected function recordEvent(DomainEvent $event): void
    {
        if (!isset($this->events)) {
            $this->resetEvents();
        }
        $this->events->add($event);
    }

    private function resetEvents(): void
    {
        $this->events = DomainEvents::of();
    }
}
