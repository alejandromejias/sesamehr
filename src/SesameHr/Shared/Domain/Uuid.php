<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain;

use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Exception\InternalServerErrorException;
use Ramsey\Uuid\Uuid as RamseyUuid;
use Stringable;

final class Uuid implements Stringable
{
    public string $value;

    public function __construct(string $value)
    {
        $this->checkIsValid($value);
        $this->value = $value;
    }

    private function checkIsValid(string $value): void
    {
        if (!self::isValid($value)) {
            $errors = ExceptionErrors::of([ExceptionError::fromInvalidValue('uuid')]);
            throw InternalServerErrorException::fromErrors($errors);
        }
    }

    public static function isValid(string $value): bool
    {
        return RamseyUuid::isValid($value);
    }

    public static function from(string $value): self
    {
        return new self($value);
    }

    public static function create(): self
    {
        return new self(RamseyUuid::uuid4()->toString());
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function value(): string
    {
        return $this->value;
    }
}
