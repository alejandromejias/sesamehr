<?php

declare(strict_types=1);

namespace App\SesameHr\Shared\Domain;

use DateTimeImmutable;
use Stringable;

final class DateTime extends DateTimeImmutable implements Stringable
{
    public const DATE_TIME_FORMAT = 'Y-m-d H:i:s';

    public static function now(): self
    {
        return self::from();
    }

    public static function from(string $datetime = 'now'): self
    {
        return new self($datetime);
    }

    public static function isValid(mixed $value): bool
    {
        return DateTimeImmutable::createFromFormat(self::DATE_TIME_FORMAT, $value) !== false;
    }

    public function __toString(): string
    {
        return $this->format(self::DATE_TIME_FORMAT);
    }
}
