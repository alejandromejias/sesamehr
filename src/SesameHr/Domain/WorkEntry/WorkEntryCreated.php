<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryCreated extends DomainEvent
{
    public function __construct(
        Uuid $eventId,
        DateTime $occurredOn,
        private WorkEntry $workEntry,
    ) {
        parent::__construct($workEntry->id(), $eventId, $occurredOn);
    }

    public static function create(WorkEntry $workEntry): self
    {
        return self::from(
            Uuid::create(),
            DateTime::now(),
            $workEntry,
        );
    }

    public static function from(
        Uuid $eventId,
        DateTime $occurredOn,
        WorkEntry $workEntry
    ): self {
        return new self(
            $eventId,
            $occurredOn,
            $workEntry,
        );
    }

    public static function eventName(): string
    {
        return 'workEntry.created';
    }

    public function workEntry(): WorkEntry
    {
        return $this->workEntry;
    }
}
