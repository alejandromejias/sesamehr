<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryDeleted extends DomainEvent
{
    public function __construct(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        private DateTime $deletedAt
    ) {
        parent::__construct($aggregateId, $eventId, $occurredOn);
    }

    public static function create(Uuid $aggregateId, DateTime $deletedAt): self
    {
        return self::from(
            $aggregateId,
            Uuid::create(),
            DateTime::now(),
            $deletedAt
        );
    }

    public static function from(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        DateTime $deletedAt
    ): self {
        return new self(
            $aggregateId,
            $eventId,
            $occurredOn,
            $deletedAt
        );
    }

    public static function eventName(): string
    {
        return 'workEntry.deleted';
    }

    public function deletedAt(): DateTime
    {
        return $this->deletedAt;
    }
}
