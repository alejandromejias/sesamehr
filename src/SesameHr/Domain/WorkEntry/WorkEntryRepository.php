<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Uuid;

interface WorkEntryRepository
{
    public function save(WorkEntry $workEntry): void;
    public function ofId(Uuid $id): ?WorkEntry;
    public function search(Criteria $criteria): WorkEntries;
}
