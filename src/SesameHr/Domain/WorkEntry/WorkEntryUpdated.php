<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class WorkEntryUpdated extends DomainEvent
{
    public function __construct(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        private DateTime $startDate,
        private ?DateTime $endDate,
        private DateTime $updatedAt
    ) {
        parent::__construct($aggregateId, $eventId, $occurredOn);
    }

    public static function create(
        Uuid $aggregateId,
        DateTime $startDate,
        ?DateTime $endDate,
        DateTime $updatedAt
    ): self {
        return self::from(
            $aggregateId,
            Uuid::create(),
            DateTime::now(),
            $startDate,
            $endDate,
            $updatedAt
        );
    }

    public static function from(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        DateTime $startDate,
        ?DateTime $endDate,
        DateTime $updatedAt
    ): self {
        return new self(
            $aggregateId,
            $eventId,
            $occurredOn,
            $startDate,
            $endDate,
            $updatedAt
        );
    }

    public static function eventName(): string
    {
        return 'workEntry.updated';
    }

    public function startDate(): DateTime
    {
        return $this->startDate;
    }

    public function endDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }
}
