<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\Aggregate\AggregateRoot;
use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final class WorkEntry extends AggregateRoot
{
    private DateTime $startDate;
    private ?DateTime $endDate;

    public function __construct(
        private readonly Uuid $id,
        private readonly Uuid $userId,
        DateTime $startDate,
        ?DateTime $endDate,
        private readonly DateTime $createdAt,
        private DateTime $updatedAt,
        private ?DateTime $deletedAt = null
    ) {
        parent::__construct();
        $this->checkWorkEntryDates($startDate, $endDate);
        $this->updateWorkEntryDates($startDate, $endDate);
    }

    private function endDateIsLessThanStartDate(
        DateTime $startDate,
        ?DateTime $endDate
    ): bool {
        return $endDate->getTimestamp() < $startDate->getTimestamp();
    }

    public static function create(
        string $userId,
        string $startDate,
        ?string $endDate
    ): self {
        $newWorkEntry = self::from(
            Uuid::create(),
            Uuid::from($userId),
            DateTime::from($startDate),
            $endDate ? DateTime::from($endDate) : null,
            DateTime::now(),
            DateTime::now()
        );
        $newWorkEntry->recordEvent(WorkEntryCreated::create($newWorkEntry));
        return $newWorkEntry;
    }

    public static function from(
        Uuid $id,
        Uuid $userId,
        DateTime $startDate,
        ?DateTime $endDate,
        DateTime $createdAt,
        DateTime $updatedAt,
        ?DateTime $deletedAt = null
    ): self {
        return new self(
            $id,
            $userId,
            $startDate,
            $endDate,
            $createdAt,
            $updatedAt,
            $deletedAt
        );
    }

    public function update(string $startDate, ?string $endDate): void
    {
        if ($this->isDeleted()) {
            $errors = ExceptionErrors::of([ExceptionError::fromCannotUpdateDeletedResource('id')]);
            throw BadRequestException::fromErrors(errors: $errors);
        }
        $startDateTime = DateTime::from($startDate);
        $endDateTime = !is_null($endDate) ? DateTime::from($endDate) : null;
        $this->checkWorkEntryDates($startDateTime, $endDateTime);
        $this->updateWorkEntryDates($startDateTime, $endDateTime);
        $this->updatedAt = DateTime::now();
        $this->recordEvent(
            WorkEntryUpdated::create(
                $this->id(),
                $this->startDate(),
                $this->endDate(),
                $this->updatedAt()
            )
        );
    }

    private function checkWorkEntryDates(
        DateTime $startDate,
        ?DateTime $endDate
    ): void {
        if (is_null($endDate)) {
            return;
        }
        if ($this->endDateIsLessThanStartDate($startDate, $endDate)) {
            $errors = ExceptionErrors::of([ExceptionError::fromInconsistentData('endDate')]);
            throw BadRequestException::fromErrors($errors);
        }
    }

    private function updateWorkEntryDates(
        DateTime $startDate,
        ?DateTime $endDate
    ): void {
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function delete(): void
    {
        if ($this->isDeleted()) {
            $errors = ExceptionErrors::of([ExceptionError::fromCannotDeleteDeletedResource('id')]);
            throw BadRequestException::fromErrors(errors: $errors);
        }
        $this->deletedAt = DateTime::now();
        $this->recordEvent(WorkEntryDeleted::create($this->id(), $this->deletedAt()));
    }

    public function isDeleted(): bool
    {
        return !is_null($this->deletedAt());
    }

    public function summary(): array
    {
        return [
            'id' => (string)$this->id(),
            'userId' => (string)$this->userId(),
            'startDate' => (string)$this->startDate(),
            'endDate' => $this->hasEndDate() ? (string)$this->endDate() : null,
            'createdAt' => (string)$this->createdAt(),
            'updatedAt' => (string)$this->updatedAt()
        ];
    }

    private function hasEndDate(): bool
    {
        return !is_null($this->endDate);
    }

    public function id(): Uuid
    {
        return $this->id;
    }

    public function userId(): Uuid
    {
        return $this->userId;
    }

    public function startDate(): DateTime
    {
        return $this->startDate;
    }

    public function endDate(): ?DateTime
    {
        return $this->endDate;
    }

    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }

    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function deletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }
}
