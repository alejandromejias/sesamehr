<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\WorkEntry;

use App\SesameHr\Shared\Domain\Collection;

final class WorkEntries extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof WorkEntry;
    }
}
