<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\User;

use App\SesameHr\Shared\Domain\Collection;

final class Users extends Collection
{
    protected function isInstanceOf(mixed $item): bool
    {
        return $item instanceof User;
    }
}
