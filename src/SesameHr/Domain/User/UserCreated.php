<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\User;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class UserCreated extends DomainEvent
{
    public function __construct(
        Uuid $eventId,
        DateTime $occurredOn,
        private User $user,
    ) {
        parent::__construct($user->id(), $eventId, $occurredOn);
    }

    public static function create(User $user): self
    {
        return self::from(
            Uuid::create(),
            DateTime::now(),
            $user,
        );
    }

    public static function from(
        Uuid $eventId,
        DateTime $occurredOn,
        User $user
    ): self {
        return new self(
            $eventId,
            $occurredOn,
            $user,
        );
    }

    public static function eventName(): string
    {
        return 'user.created';
    }

    public function user(): User
    {
        return $this->user;
    }
}
