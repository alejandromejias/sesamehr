<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\User;

use App\SesameHr\Shared\Domain\Aggregate\AggregateRoot;
use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Email;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Domain\Exception\ExceptionErrors;
use App\SesameHr\Shared\Domain\Uuid;

final class User extends AggregateRoot
{
    public function __construct(
        private readonly Uuid $id,
        private string $name,
        private Email $email,
        private readonly DateTime $createdAt,
        private DateTime $updatedAt,
        private ?DateTime $deletedAt = null
    ) {
        parent::__construct();
    }

    public static function create(string $name, string $email): self
    {
        $newUser = self::from(
            Uuid::create(),
            $name,
            Email::from($email),
            DateTime::now(),
            DateTime::now()
        );
        $newUser->recordEvent(UserCreated::create($newUser));
        return $newUser;
    }

    public static function from(
        Uuid $id,
        string $name,
        Email $email,
        DateTime $createdAt,
        DateTime $updatedAt,
        ?DateTime $deletedAt = null,
    ): self {
        return new self(
            $id,
            $name,
            $email,
            $createdAt,
            $updatedAt,
            $deletedAt
        );
    }

    public function update(string $name, string $email): void
    {
        if ($this->isDeleted()) {
            $errors = ExceptionErrors::of([ExceptionError::fromCannotUpdateDeletedResource('id')]);
            throw BadRequestException::fromErrors(errors: $errors);
        }
        $this->name = $name;
        $this->email = new Email($email);
        $this->updatedAt = DateTime::now();
        $this->recordEvent(
            UserUpdated::create(
                $this->id(),
                $this->name(),
                $this->email(),
                $this->updatedAt()
            )
        );
    }

    public function delete(): void
    {
        if ($this->isDeleted()) {
            $errors = ExceptionErrors::of([ExceptionError::fromCannotDeleteDeletedResource('id')]);
            throw BadRequestException::fromErrors(errors: $errors);
        }
        $this->deletedAt = DateTime::now();
        $this->recordEvent(UserDeleted::create($this->id(), $this->deletedAt()));
    }

    public function isDeleted(): bool
    {
        return !is_null($this->deletedAt());
    }

    public function summary(): array
    {
        return [
            'id' => (string)$this->id(),
            'name' => $this->name(),
            'email' => (string)$this->email(),
            'createdAt' => (string)$this->createdAt(),
            'updatedAt' => (string)$this->updatedAt()
        ];
    }

    public function id(): Uuid
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function createdAt(): DateTime
    {
        return $this->createdAt;
    }

    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function deletedAt(): ?DateTime
    {
        return $this->deletedAt;
    }
}
