<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\User;

use App\SesameHr\Shared\Domain\DateTime;
use App\SesameHr\Shared\Domain\Email;
use App\SesameHr\Shared\Domain\Event\DomainEvent;
use App\SesameHr\Shared\Domain\Uuid;

final readonly class UserUpdated extends DomainEvent
{
    public function __construct(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        private string $name,
        private Email $email,
        private DateTime $updatedAt
    ) {
        parent::__construct($aggregateId, $eventId, $occurredOn);
    }

    public static function create(
        Uuid $aggregateId,
        string $name,
        Email $email,
        DateTime $updatedAt
    ): self {
        return self::from(
            $aggregateId,
            Uuid::create(),
            DateTime::now(),
            $name,
            $email,
            $updatedAt
        );
    }

    public static function from(
        Uuid $aggregateId,
        Uuid $eventId,
        DateTime $occurredOn,
        string $name,
        Email $email,
        DateTime $updatedAt
    ): self {
        return new self(
            $aggregateId,
            $eventId,
            $occurredOn,
            $name,
            $email,
            $updatedAt
        );
    }

    public static function eventName(): string
    {
        return 'user.updated';
    }

    public function updatedAt(): DateTime
    {
        return $this->updatedAt;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function email(): Email
    {
        return $this->email;
    }
}
