<?php

declare(strict_types=1);

namespace App\SesameHr\Domain\User;

use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Uuid;

interface UserRepository
{
    public function save(User $user): void;
    public function ofId(Uuid $id): ?User;
    public function search(Criteria $criteria): Users;
}
