<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\User;

use App\SesameHr\Application\User\GetAll\UserGetAllUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

final readonly class UserGetAllController extends Controller
{
    public function __construct(
        private UserGetAllUseCase $userGetAll
    ) {
    }

    public function __invoke(): JsonResponse
    {
        $response = ($this->userGetAll)();
        return $this->ok($response->data());
    }
}
