<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\User;

use App\SesameHr\Application\User\GetById\UserGetByIdRequest;
use App\SesameHr\Application\User\GetById\UserGetByIdUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\User\UserGetByIdValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final readonly class UserGetByIdController extends Controller
{
    public function __construct(
        private UserGetByIdValidator $validator,
        private UserGetByIdUseCase $userGetById
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validate($request);
        $userGetByIdRequest = new UserGetByIdRequest(
            $request->get('id')
        );
        $response = ($this->userGetById)($userGetByIdRequest);
        return $this->ok($response->data());
    }
}
