<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\User;

use App\SesameHr\Application\User\Delete\UserDeleteRequest;
use App\SesameHr\Application\User\Delete\UserDeleteUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\User\UserDeleteValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final readonly class UserDeleteController extends Controller
{
    public function __construct(
        private UserDeleteValidator $validator,
        private UserDeleteUseCase $userDelete
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $this->validator->validate($request);
        $userDeleteRequest = new UserDeleteRequest(
            $request->get('id')
        );
        ($this->userDelete)($userDeleteRequest);
        return $this->noContent();
    }
}
