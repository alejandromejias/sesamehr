<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\User;

use App\SesameHr\Application\User\Update\UserUpdateRequest;
use App\SesameHr\Application\User\Update\UserUpdateUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\User\UserUpdateValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final readonly class UserUpdateController extends Controller
{
    public function __construct(
        private UserUpdateValidator $validator,
        private UserUpdateUseCase $userUpdate
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $this->validator->validate($request);
        $userUpdateRequest = new UserUpdateRequest(
            $request->get('id'),
            $request->getPayload()->get('name'),
            $request->getPayload()->get('email'),
        );
        ($this->userUpdate)($userUpdateRequest);
        return $this->noContent();
    }
}
