<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\User;

use App\SesameHr\Application\User\Create\UserCreateRequest;
use App\SesameHr\Application\User\Create\UserCreateUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\User\UserCreateValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final readonly class UserCreateController extends Controller
{
    public function __construct(
        private UserCreateValidator $validator,
        private UserCreateUseCase $userCreate
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validate($request);
        $userCreateRequest = new UserCreateRequest(
            $request->getPayload()->get('name'),
            $request->getPayload()->get('email'),
        );
        $response = ($this->userCreate)($userCreateRequest);
        return $this->created($response->data());
    }
}
