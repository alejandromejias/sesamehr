<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

abstract readonly class Controller
{
    public function ok(array $data): JsonResponse
    {
        return new JsonResponse($data, Response::HTTP_OK);
    }

    public function created(array $data): JsonResponse
    {
        return new JsonResponse($data, Response::HTTP_CREATED);
    }

    public function noContent(): Response
    {
        return new Response(status: Response::HTTP_NO_CONTENT);
    }
}
