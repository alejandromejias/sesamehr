<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\WorkEntry;

use App\SesameHr\Application\WorkEntry\GetByUserId\WorkEntryGetByUserIdRequest;
use App\SesameHr\Application\WorkEntry\GetByUserId\WorkEntryGetByUserIdUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\User\WorkEntryGetByUserIdValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final readonly class WorkEntryGetByUserIdController extends Controller
{
    public function __construct(
        private WorkEntryGetByUserIdValidator $validator,
        private WorkEntryGetByUserIdUseCase $workEntryGetByUserId
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validate($request);
        $workEntryGetByUserIdRequest = new WorkEntryGetByUserIdRequest(
            $request->get('id')
        );
        $response = ($this->workEntryGetByUserId)($workEntryGetByUserIdRequest);
        return $this->ok($response->data());
    }
}
