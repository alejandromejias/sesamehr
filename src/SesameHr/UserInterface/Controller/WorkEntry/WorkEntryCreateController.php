<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\WorkEntry;

use App\SesameHr\Application\WorkEntry\Create\WorkEntryCreateRequest;
use App\SesameHr\Application\WorkEntry\Create\WorkEntryCreateUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\WorkEntry\WorkEntryCreateValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final readonly class WorkEntryCreateController extends Controller
{
    public function __construct(
        private WorkEntryCreateValidator $validator,
        private WorkEntryCreateUseCase $workEntryCreate
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validate($request);
        $workEntryCreateRequest = new WorkEntryCreateRequest(
            $request->getPayload()->get('userId'),
            $request->getPayload()->get('startDate'),
            $request->getPayload()->get('endDate')
        );
        $response = ($this->workEntryCreate)($workEntryCreateRequest);
        return $this->created($response->data());
    }
}
