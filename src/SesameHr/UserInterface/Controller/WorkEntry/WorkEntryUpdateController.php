<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\WorkEntry;

use App\SesameHr\Application\WorkEntry\Update\WorkEntryUpdateRequest;
use App\SesameHr\Application\WorkEntry\Update\WorkEntryUpdateUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\WorkEntry\WorkEntryUpdateValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final readonly class WorkEntryUpdateController extends Controller
{
    public function __construct(
        private WorkEntryUpdateValidator $validator,
        private WorkEntryUpdateUseCase $workEntryUpdate
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $this->validator->validate($request);
        $workEntryUpdateRequest = new WorkEntryUpdateRequest(
            $request->get('id'),
            $request->getPayload()->get('startDate'),
            $request->getPayload()->get('endDate')
        );
        ($this->workEntryUpdate)($workEntryUpdateRequest);
        return $this->noContent();
    }
}
