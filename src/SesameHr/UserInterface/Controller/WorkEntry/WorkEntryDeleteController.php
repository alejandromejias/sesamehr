<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\WorkEntry;

use App\SesameHr\Application\WorkEntry\Delete\WorkEntryDeleteRequest;
use App\SesameHr\Application\WorkEntry\Delete\WorkEntryDeleteUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\WorkEntry\WorkEntryDeleteValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final readonly class WorkEntryDeleteController extends Controller
{
    public function __construct(
        private WorkEntryDeleteValidator $validator,
        private WorkEntryDeleteUseCase $workEntryDelete
    ) {
    }

    public function __invoke(Request $request): Response
    {
        $this->validator->validate($request);
        $workEntryDeleteRequest = new WorkEntryDeleteRequest(
            $request->get('id')
        );
        ($this->workEntryDelete)($workEntryDeleteRequest);
        return $this->noContent();
    }
}
