<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Controller\WorkEntry;

use App\SesameHr\Application\WorkEntry\GetById\WorkEntryGetByIdRequest;
use App\SesameHr\Application\WorkEntry\GetById\WorkEntryGetByIdUseCase;
use App\SesameHr\UserInterface\Controller\Controller;
use App\SesameHr\UserInterface\Validation\WorkEntry\WorkEntryGetByIdValidator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

final readonly class WorkEntryGetByIdController extends Controller
{
    public function __construct(
        private WorkEntryGetByIdValidator $validator,
        private WorkEntryGetByIdUseCase $workEntryGetById
    ) {
    }

    public function __invoke(Request $request): JsonResponse
    {
        $this->validator->validate($request);
        $workEntryGetByIdRequest = new WorkEntryGetByIdRequest(
            $request->get('id')
        );
        $response = ($this->workEntryGetById)($workEntryGetByIdRequest);
        return $this->ok($response->data());
    }
}
