<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Validation\User;

use App\SesameHr\Shared\Infrastructure\Validation\Validator;
use App\SesameHr\UserInterface\Validation\EmailRule;
use App\SesameHr\UserInterface\Validation\RequiredRule;
use App\SesameHr\UserInterface\Validation\UuidRule;
use Symfony\Component\HttpFoundation\Request;

class UserUpdateValidator extends Validator
{
    public function validate(Request $request): void
    {
        $id = $request->get('id');
        $name = $request->getPayload()->get('name');
        $email = $request->getPayload()->get('email');
        $this->context
            ->addRule(new UuidRule($id, 'id'))
            ->addRule(new RequiredRule($name, 'name'))
            ->addRule(new RequiredRule($email, 'email'))
            ->addRule(new EmailRule($email, 'email'))
            ->validate();
    }
}
