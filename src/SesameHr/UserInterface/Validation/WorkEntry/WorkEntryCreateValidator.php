<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Validation\WorkEntry;

use App\SesameHr\Shared\Infrastructure\Validation\Validator;
use App\SesameHr\UserInterface\Validation\DateTimeRule;
use App\SesameHr\UserInterface\Validation\RequiredRule;
use App\SesameHr\UserInterface\Validation\UuidRule;
use Symfony\Component\HttpFoundation\Request;

class WorkEntryCreateValidator extends Validator
{
    public function validate(Request $request): void
    {
        $userId = $request->getPayload()->get('userId');
        $startDate = $request->getPayload()->get('startDate');
        $endDate = $request->getPayload()->get('endDate');
        $this->context
            ->addRule(new RequiredRule($startDate, 'userId'))
            ->addRule(new UuidRule($userId, 'userId'))
            ->addRule(new RequiredRule($startDate, 'startDate'))
            ->addRule(new DateTimeRule($startDate, 'startDate'))
            ->addRule(new DateTimeRule($endDate, 'endDate'))
            ->validate();
    }
}
