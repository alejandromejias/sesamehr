<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Validation\WorkEntry;

use App\SesameHr\Shared\Infrastructure\Validation\Validator;
use App\SesameHr\UserInterface\Validation\UuidRule;
use Symfony\Component\HttpFoundation\Request;

final class WorkEntryGetByIdValidator extends Validator
{
    public function validate(Request $request): void
    {
        $id = $request->get('id');
        $this->context
            ->addRule(new UuidRule($id, 'id'))
            ->validate();
    }
}
