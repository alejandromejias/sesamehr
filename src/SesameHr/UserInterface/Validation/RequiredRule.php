<?php

declare(strict_types=1);

namespace App\SesameHr\UserInterface\Validation;

use App\SesameHr\Shared\Domain\Exception\ExceptionError;
use App\SesameHr\Shared\Infrastructure\Validation\ValidationRule;

class RequiredRule extends ValidationRule
{
    protected function checkRule(mixed $value, string $target): void
    {
        if (is_null($value)) {
            $this->errors->add(ExceptionError::fromNullValue($target));
        }
    }
}
