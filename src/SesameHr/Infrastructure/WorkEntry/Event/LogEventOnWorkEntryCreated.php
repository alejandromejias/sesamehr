<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\WorkEntry\Event;

use App\SesameHr\Domain\WorkEntry\WorkEntryCreated;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnWorkEntryCreated
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(WorkEntryCreated $workEntryCreated): void
    {
        $this->logger->notice(
            sprintf('A new work entry with id %s was created!', $workEntryCreated->workEntry()->id()),
            ['workEntry' => $workEntryCreated->workEntry()->summary()]
        );
    }
}
