<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\WorkEntry\Event;

use App\SesameHr\Domain\WorkEntry\WorkEntryUpdated;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnWorkEntryUpdated
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(WorkEntryUpdated $workEntryUpdated): void
    {
        $this->logger->notice(
            sprintf('A work entry with id %s was updated!', $workEntryUpdated->aggregatedId()),
            [
                'workEntry' => [
                    'id' => (string)$workEntryUpdated->aggregatedId(),
                    'startDate' => (string)$workEntryUpdated->startDate(),
                    'endDate' => (string)$workEntryUpdated->endDate(),
                    'updatedAt' => (string)$workEntryUpdated->updatedAt()
                ]
            ]
        );
    }
}
