<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\WorkEntry\Event;

use App\SesameHr\Domain\WorkEntry\WorkEntryDeleted;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnWorkEntryDeleted
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(WorkEntryDeleted $workEntryDeleted): void
    {
        $this->logger->notice(
            sprintf('A work entry with id %s was deleted!', $workEntryDeleted->aggregatedId()),
            [
                'workEntry' => [
                    'id' => (string)$workEntryDeleted->aggregatedId(),
                    'deletedAt' => (string)$workEntryDeleted->deletedAt()
                ]
            ]
        );
    }
}
