<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\WorkEntry\Doctrine;

use App\SesameHr\Domain\WorkEntry\WorkEntries;
use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Uuid;
use App\SesameHr\Shared\Infrastructure\Doctrine\DoctrineRepository;

final class DoctrineWorkEntryRepository extends DoctrineRepository implements WorkEntryRepository
{
    public function save(WorkEntry $workEntry): void
    {
        $this->persist($workEntry);
    }

    public function ofId(Uuid $id): ?WorkEntry
    {
        return $this->repository(WorkEntry::class)->find($id);
    }

    public function search(Criteria $criteria): WorkEntries
    {
        $collection = $this
            ->repository(WorkEntry::class)
            ->matching($this->createDoctrineCriteria($criteria));
        return WorkEntries::of($collection->toArray());
    }
}
