<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\User\Event;

use App\SesameHr\Domain\User\UserUpdated;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnUserUpdated
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(UserUpdated $userUpdated): void
    {
        $this->logger->notice(
            sprintf('A user with id %s was updated!', $userUpdated->aggregatedId()),
            [
                'user' => [
                    'id' => (string)$userUpdated->aggregatedId(),
                    'name' => $userUpdated->name(),
                    'email' => (string)$userUpdated->email(),
                    'updatedAt' => (string)$userUpdated->updatedAt()
                ]
            ]
        );
    }
}
