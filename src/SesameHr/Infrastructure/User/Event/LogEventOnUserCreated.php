<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\User\Event;

use App\SesameHr\Domain\User\UserCreated;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnUserCreated
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(UserCreated $userCreated): void
    {
        $this->logger->notice(
            sprintf('A new user with id %s was created!', $userCreated->user()->id()),
            ['user' => $userCreated->user()->summary()]
        );
    }
}
