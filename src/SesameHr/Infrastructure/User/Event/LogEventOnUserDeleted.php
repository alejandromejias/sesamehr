<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\User\Event;

use App\SesameHr\Domain\User\UserDeleted;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
final readonly class LogEventOnUserDeleted
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(UserDeleted $userDeleted): void
    {
        $this->logger->notice(
            sprintf('A user with id %s was deleted!', $userDeleted->aggregatedId()),
            [
                'user' => [
                    'id' => (string)$userDeleted->aggregatedId(),
                    'deletedAt' => (string)$userDeleted->deletedAt()
                ]
            ]
        );
    }
}
