<?php

declare(strict_types=1);

namespace App\SesameHr\Infrastructure\User\Doctrine;

use App\SesameHr\Domain\User\User;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\User\Users;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Uuid;
use App\SesameHr\Shared\Infrastructure\Doctrine\DoctrineRepository;

final class DoctrineUserRepository extends DoctrineRepository implements UserRepository
{
    public function save(User $user): void
    {
        $this->persist($user);
    }

    public function ofId(Uuid $id): ?User
    {
        return $this->repository(User::class)->find($id);
    }

    public function search(Criteria $criteria): Users
    {
        $collection = $this
            ->repository(User::class)
            ->matching($this->createDoctrineCriteria($criteria));
        return Users::of($collection->toArray());
    }
}
