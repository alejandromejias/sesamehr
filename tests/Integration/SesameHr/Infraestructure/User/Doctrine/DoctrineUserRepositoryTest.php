<?php

declare(strict_types=1);

namespace App\Tests\Integration\SesameHr\Infraestructure\User\Doctrine;

use App\SesameHr\Infrastructure\User\Doctrine\DoctrineUserRepository;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\Filter;
use App\SesameHr\Shared\Domain\Criteria\Filters;
use App\Tests\Integration\TestCase;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use PHPUnit\Framework\Attributes\Test;

class DoctrineUserRepositoryTest extends TestCase
{
    private DoctrineUserRepository $userRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new DoctrineUserRepository($this->entityManager);
    }

    #[Test]
    public function shouldSaveUser(): void
    {
        $user = UserMother::random();

        $this->userRepository->save($user);

        $this->assertEquals($user, $this->userRepository->ofId($user->id()));
    }

    #[Test]
    public function shouldSearchUserWithResults(): void
    {
        $user = UserMother::random();
        $this->userRepository->save($user);
        $filter = Filter::fromEqual('email', (string)$user->email());
        $criteria = Criteria::from(Filters::of([$filter]));

        $users = $this->userRepository->search($criteria);

        $this->assertEquals($user, $users->getIterator()->current());
    }

    #[Test]
    public function shouldSearchUserWithoutResults(): void
    {
        $user = UserMother::random();
        $this->userRepository->save($user);
        $filter = Filter::fromEqual('email', StringMother::email());
        $criteria = Criteria::from(Filters::of([$filter]));

        $users = $this->userRepository->search($criteria);

        $this->assertTrue($users->isEmpty());
    }
}
