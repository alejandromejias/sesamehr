<?php

declare(strict_types=1);

namespace App\Tests\Integration\SesameHr\Infraestructure\WorkEntry\Doctrine;

use App\SesameHr\Infrastructure\User\Doctrine\DoctrineUserRepository;
use App\SesameHr\Infrastructure\WorkEntry\Doctrine\DoctrineWorkEntryRepository;
use App\SesameHr\Shared\Domain\Criteria\Criteria;
use App\SesameHr\Shared\Domain\Criteria\Filter;
use App\SesameHr\Shared\Domain\Criteria\Filters;
use App\Tests\Integration\TestCase;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntryMother;
use PHPUnit\Framework\Attributes\Test;

final class DoctrineWorkEntryRepositoryTest extends TestCase
{
    private DoctrineUserRepository $userRepository;
    private DoctrineWorkEntryRepository $workEntryRepository;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userRepository = new DoctrineUserRepository($this->entityManager);
        $this->workEntryRepository = new DoctrineWorkEntryRepository($this->entityManager);
    }

    #[Test]
    public function shouldSaveWorkEntry(): void
    {
        $user = UserMother::random();
        $this->userRepository->save($user);
        $workEntry = WorkEntryMother::random(['userId' => $user->id()]);

        $this->workEntryRepository->save($workEntry);

        $this->assertEquals($workEntry, $this->workEntryRepository->ofId($workEntry->id()));
    }

    #[Test]
    public function shouldSearchWorkEntriesWithResults(): void
    {
        $user = UserMother::random();
        $this->userRepository->save($user);
        $workEntry = WorkEntryMother::random(['userId' => $user->id()]);
        $this->workEntryRepository->save($workEntry);
        $filter = Filter::fromEqual('userId', (string)$user->id());
        $criteria = Criteria::from(Filters::of([$filter]));

        $workEntries = $this->workEntryRepository->search($criteria);

        $this->assertEquals($workEntry, $workEntries->getIterator()->current());
    }

    #[Test]
    public function shouldSearchWorkEntriesWithoutResults(): void
    {
        $user = UserMother::random();
        $this->userRepository->save($user);
        $workEntry = WorkEntryMother::random(['userId' => $user->id()]);
        $this->workEntryRepository->save($workEntry);
        $filter = Filter::fromEqual('userId', StringMother::uuid());
        $criteria = Criteria::from(Filters::of([$filter]));

        $workEntries = $this->workEntryRepository->search($criteria);

        $this->assertTrue($workEntries->isEmpty());
    }
}
