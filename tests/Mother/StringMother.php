<?php

declare(strict_types=1);

namespace App\Tests\Mother;

use App\SesameHr\Shared\Domain\DateTime;

final class StringMother
{
    public static function uuid(): string
    {
        return Mother::instance()->uuid();
    }

    public static function name(): string
    {
        return Mother::instance()->name();
    }

    public static function email(): string
    {
        return Mother::instance()->email();
    }

    public static function datetime(): string
    {
        return Mother::instance()->dateTime()->format(DateTime::DATE_TIME_FORMAT);
    }

    public static function datetimeInterval(string $startDate, string $endDate): string
    {
        return Mother::instance()->dateTimeInInterval($startDate, $endDate)->format(DateTime::DATE_TIME_FORMAT);
    }
}
