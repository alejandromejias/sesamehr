<?php

declare(strict_types=1);

namespace App\Tests\Mother;

use Faker\Factory;
use Faker\Generator;

final class Mother
{
    private static ?Generator $instance = null;

    public static function instance(): Generator
    {
        if (static::$instance === null) {
            static::$instance = Factory::create();
        }
        return static::$instance;
    }
}
