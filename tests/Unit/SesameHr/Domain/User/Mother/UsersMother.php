<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Domain\User\Mother;

use App\SesameHr\Domain\User\Users;

final class UsersMother
{
    public static function random(): Users
    {
        return self::of([UserMother::random()]);
    }

    public static function of(array $items = []): Users
    {
        return Users::of($items);
    }
}
