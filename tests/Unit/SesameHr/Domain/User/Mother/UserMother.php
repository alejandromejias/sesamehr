<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Domain\User\Mother;

use App\SesameHr\Domain\User\User;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\EmailMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\UuidMother;

final class UserMother
{
    public static function random(array $parameters = []): User
    {
        $arguments = [
            'id' => $parameters['id'] ?? UuidMother::random(),
            'name' => $parameters['name'] ?? StringMother::name(),
            'email' => $parameters['email'] ?? EmailMother::random(),
            'createdAt' => $parameters['createdAt'] ?? DateTimeMother::random(),
            'updatedAt' => $parameters['updatedAt'] ?? DateTimeMother::random(),
            'deletedAt' => $parameters['deletedAt'] ?? null,
        ];
        return User::from(...array_values($arguments));
    }
}
