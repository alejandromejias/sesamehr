<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother;

use App\SesameHr\Domain\WorkEntry\WorkEntries;

final class WorkEntriesMother
{
    public static function random(): WorkEntries
    {
        return self::of([WorkEntryMother::random()]);
    }

    public static function of(array $items = []): WorkEntries
    {
        return WorkEntries::of($items);
    }
}
