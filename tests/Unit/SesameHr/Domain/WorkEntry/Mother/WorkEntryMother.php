<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother;

use App\SesameHr\Domain\WorkEntry\WorkEntry;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\UuidMother;

final class WorkEntryMother
{
    public static function random(array $parameters = []): WorkEntry
    {
        $arguments = [
            'id' => $parameters['id'] ?? UuidMother::random(),
            'userId' => $parameters['userId'] ?? UuidMother::random(),
            'startDate' => $parameters['startDate'] ?? DateTimeMother::random(),
            'endDate' => $parameters['endDate'] ?? null,
            'createdAt' => $parameters['createdAt'] ?? DateTimeMother::random(),
            'updatedAt' => $parameters['updatedAt'] ?? DateTimeMother::random(),
            'deletedAt' => $parameters['deletedAt'] ?? null
        ];
        return WorkEntry::from(...array_values($arguments));
    }
}
