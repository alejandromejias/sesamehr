<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\WorkEntry\Delete;

use App\SesameHr\Application\WorkEntry\Delete\WorkEntryDeleteRequest;
use App\SesameHr\Application\WorkEntry\Delete\WorkEntryDeleteUseCase;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntryMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class WorkEntryDeleteUseCaseTest extends TestCase
{
    private WorkEntryRepository $workEntryRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->workEntryRepository = $this->createMock(WorkEntryRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldDeleteWorkEntry(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(WorkEntryMother::random());
        $this->workEntryRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $request = new WorkEntryDeleteRequest(StringMother::uuid());

        (new WorkEntryDeleteUseCase($this->workEntryRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenWorkEntryNotExists(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(null);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryDeleteRequest(StringMother::uuid());

        $this->expectException(BadRequestException::class);

        (new WorkEntryDeleteUseCase($this->workEntryRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenWorkEntryIsDeleted(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(WorkEntryMother::random(['deletedAt' => DateTimeMother::random()]));
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryDeleteRequest(StringMother::uuid());

        $this->expectException(BadRequestException::class);

        (new WorkEntryDeleteUseCase($this->workEntryRepository, $this->eventBus))($request);
    }
}
