<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\WorkEntry\Update;

use App\SesameHr\Application\WorkEntry\Update\WorkEntryUpdateRequest;
use App\SesameHr\Application\WorkEntry\Update\WorkEntryUpdateUseCase;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntryMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class WorkEntryUpdateUseCaseTest extends TestCase
{
    private WorkEntryRepository $workEntryRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->workEntryRepository = $this->createMock(WorkEntryRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldUpdateWorkEntry(): void
    {
        $workEntry = WorkEntryMother::random();
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn($workEntry);
        $this->workEntryRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $request = new WorkEntryUpdateRequest(
            (string)$workEntry->id(),
            $startDate = StringMother::datetime(),
            StringMother::datetimeInterval($startDate, '+8 hours')
        );

        $response = (new WorkEntryUpdateUseCase($this->workEntryRepository, $this->eventBus))($request);

        $this->assertEquals($request->startDate(), $response->data()['startDate']);
        $this->assertEquals($request->endDate(), $response->data()['endDate']);
    }

    #[Test]
    public function shouldThrowErrorWhenWorkEntryNotExists(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(null);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryUpdateRequest(StringMother::uuid(), StringMother::datetime());

        $this->expectException(BadRequestException::class);

        (new WorkEntryUpdateUseCase($this->workEntryRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenWorkEntryIsDeleted(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(WorkEntryMother::random(['deletedAt' => DateTimeMother::random()]));
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryUpdateRequest(StringMother::uuid(), StringMother::datetime());

        $this->expectException(BadRequestException::class);

        (new WorkEntryUpdateUseCase($this->workEntryRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenEndDateIsLessThanStartDate(): void
    {
        $workEntry = WorkEntryMother::random();
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn($workEntry);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryUpdateRequest(
            (string)$workEntry->id(),
            $startDate = StringMother::datetime(),
            StringMother::datetimeInterval($startDate, '-1 hours')
        );

        $this->expectException(BadRequestException::class);

        (new WorkEntryUpdateUseCase($this->workEntryRepository, $this->eventBus))($request);
    }
}
