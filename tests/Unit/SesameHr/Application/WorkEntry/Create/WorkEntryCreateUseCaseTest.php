<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\WorkEntry\Create;

use App\SesameHr\Application\WorkEntry\Create\WorkEntryCreateRequest;
use App\SesameHr\Application\WorkEntry\Create\WorkEntryCreateUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntryMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class WorkEntryCreateUseCaseTest extends TestCase
{
    private UserRepository $userRepository;
    private WorkEntryRepository $workEntryRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->workEntryRepository = $this->createMock(WorkEntryRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldCreateNewWorkEntry(): void
    {
        $user = UserMother::random();
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $this->workEntryRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $workEntry = WorkEntryMother::random();
        $request = new WorkEntryCreateRequest((string)$user->id(), (string)$workEntry->createdAt());

        $response = (new WorkEntryCreateUseCase(
            $this->userRepository,
            $this->workEntryRepository,
            $this->eventBus
        ))($request);

        $this->assertArrayHasKey('id', $response->data());
        $this->assertEquals((string)$user->id(), $response->data()['userId']);
        $this->assertEquals((string)$workEntry->startDate(), $response->data()['startDate']);
        $this->assertNull($response->data()['endDate']);
        $this->assertArrayHasKey('createdAt', $response->data());
        $this->assertArrayHasKey('updatedAt', $response->data());
    }

    #[Test]
    public function shouldThrowErrorWhenUserNotExists(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(null);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new WorkEntryCreateRequest(StringMother::uuid(), StringMother::datetime());

        $this->expectException(BadRequestException::class);

        (new WorkEntryCreateUseCase($this->userRepository, $this->workEntryRepository, $this->eventBus))($request);
    }
}
