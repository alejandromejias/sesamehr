<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\WorkEntry\GetByUserId;

use App\SesameHr\Application\WorkEntry\GetByUserId\WorkEntryGetByUserIdRequest;
use App\SesameHr\Application\WorkEntry\GetByUserId\WorkEntryGetByUserIdUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntriesMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class WorkEntryGetByUserIdUseCaseTest extends TestCase
{
    private UserRepository $userRepository;
    private WorkEntryRepository $workEntryRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->workEntryRepository = $this->createMock(WorkEntryRepository::class);
    }

    #[Test]
    public function shouldGetWorkEntryByUserIdWithResults(): void
    {
        $user = UserMother::random();
        $workEntries = WorkEntriesMother::random();
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $this->workEntryRepository
            ->method('search')
            ->willReturn($workEntries);
        $request = new WorkEntryGetByUserIdRequest((string)$user->id());

        $response = (new WorkEntryGetByUserIdUseCase($this->userRepository, $this->workEntryRepository))($request);

        $this->assertEquals($workEntries->getIterator()->current()->summary(), current($response->data()));
    }

    #[Test]
    public function shouldGetWorkEntryByUserIdWithoutResults(): void
    {
        $user = UserMother::random();
        $workEntries = WorkEntriesMother::of();
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $this->workEntryRepository
            ->method('search')
            ->willReturn($workEntries);
        $request = new WorkEntryGetByUserIdRequest((string)$user->id());

        $response = (new WorkEntryGetByUserIdUseCase($this->userRepository, $this->workEntryRepository))($request);

        $this->assertEmpty($response->data());
    }

    #[Test]
    public function shouldThrowErrorWhenUserNotExists(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(null);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('search');
        $request = new WorkEntryGetByUserIdRequest(StringMother::uuid());

        $this->expectException(BadRequestException::class);

        (new WorkEntryGetByUserIdUseCase($this->userRepository, $this->workEntryRepository))($request);
    }
}
