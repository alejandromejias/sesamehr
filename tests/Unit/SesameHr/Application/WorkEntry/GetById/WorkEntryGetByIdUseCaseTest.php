<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\WorkEntry\GetById;

use App\SesameHr\Application\WorkEntry\GetById\WorkEntryGetByIdRequest;
use App\SesameHr\Application\WorkEntry\GetById\WorkEntryGetByIdUseCase;
use App\SesameHr\Domain\WorkEntry\WorkEntryRepository;
use App\SesameHr\Shared\Domain\Exception\NotFoundException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\WorkEntry\Mother\WorkEntryMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class WorkEntryGetByIdUseCaseTest extends TestCase
{
    private WorkEntryRepository $workEntryRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->workEntryRepository = $this->createMock(WorkEntryRepository::class);
    }

    #[Test]
    public function shouldGetWorkEntryById(): void
    {
        $workEntry = WorkEntryMother::random();
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn($workEntry);
        $request = new WorkEntryGetByIdRequest((string)$workEntry->id());

        $response = (new WorkEntryGetByIdUseCase($this->workEntryRepository))($request);

        $this->assertEquals($workEntry->summary(), $response->data());
    }

    #[Test]
    public function shouldThrowErrorWhenWorkEntryNotExists(): void
    {
        $this->workEntryRepository
            ->method('ofId')
            ->willReturn(null);
        $this->workEntryRepository
            ->expects($this->never())
            ->method('save');
        $request = new WorkEntryGetByIdRequest(StringMother::uuid());

        $this->expectException(NotFoundException::class);

        (new WorkEntryGetByIdUseCase($this->workEntryRepository))($request);
    }
}
