<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\User\Update;

use App\SesameHr\Application\User\Update\UserUpdateRequest;
use App\SesameHr\Application\User\Update\UserUpdateUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class UserUpdateUseCaseTest extends TestCase
{
    private UserRepository $userRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldUpdateUser(): void
    {
        $user = UserMother::random();
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $this->userRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $request = new UserUpdateRequest((string)$user->id(), StringMother::name(), StringMother::email());

        $response = (new UserUpdateUseCase($this->userRepository, $this->eventBus))($request);

        $this->assertEquals($request->name(), $response->data()['name']);
        $this->assertEquals($request->email(), $response->data()['email']);
    }

    #[Test]
    public function shouldThrowErrorWhenUserNotExists(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(null);
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new UserUpdateRequest(StringMother::uuid(), StringMother::name(), StringMother::email());

        $this->expectException(BadRequestException::class);

        (new UserUpdateUseCase($this->userRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenUserIsDeleted(): void
    {
        $user = UserMother::random(['deletedAt' => DateTimeMother::random()]);
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new UserUpdateRequest((string)$user->id(), StringMother::name(), StringMother::email());

        $this->expectException(BadRequestException::class);

        (new UserUpdateUseCase($this->userRepository, $this->eventBus))($request);
    }
}
