<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\User\GetAll;

use App\SesameHr\Application\User\GetAll\UserGetAllUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UsersMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class UserGetAllUseCaseTest extends TestCase
{
    private UserRepository $userRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
    }

    #[Test]
    public function shouldGetAllUsers(): void
    {
        $users = UsersMother::random();
        $this->userRepository
            ->method('search')
            ->willReturn($users);

        $response = (new UserGetAllUseCase($this->userRepository))();

        foreach ($users as $index => $user) {
            $this->assertEquals((string)$user->id(), $response->data()[$index]['id']);
            $this->assertEquals($user->name(), $response->data()[$index]['name']);
            $this->assertEquals((string)$user->email(), $response->data()[$index]['email']);
            $this->assertEquals((string)$user->createdAt(), $response->data()[$index]['createdAt']);
            $this->assertEquals((string)$user->updatedAt(), $response->data()[$index]['updatedAt']);
        }
    }

    #[Test]
    public function shouldGetEmptyUsers(): void
    {
        $this->userRepository
            ->method('search')
            ->willReturn(UsersMother::of());

        $response = (new UserGetAllUseCase($this->userRepository))();

        $this->assertEmpty($response->data());
    }
}
