<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\User\Delete;

use App\SesameHr\Application\User\Delete\UserDeleteRequest;
use App\SesameHr\Application\User\Delete\UserDeleteUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Shared\Domain\Mother\DateTimeMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class UserDeleteUseCaseTest extends TestCase
{
    private UserRepository $userRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldDeleteUser(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(UserMother::random());
        $this->userRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $request = new UserDeleteRequest(StringMother::uuid());

        (new UserDeleteUseCase($this->userRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenUserNotExists(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(null);
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new UserDeleteRequest(StringMother::uuid());

        $this->expectException(BadRequestException::class);

        (new UserDeleteUseCase($this->userRepository, $this->eventBus))($request);
    }

    #[Test]
    public function shouldThrowErrorWhenUserIsDeleted(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(UserMother::random(['deletedAt' => DateTimeMother::random()]));
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new UserDeleteRequest(StringMother::uuid());

        $this->expectException(BadRequestException::class);

        (new UserDeleteUseCase($this->userRepository, $this->eventBus))($request);
    }
}
