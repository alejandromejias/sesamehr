<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\User\Create;

use App\SesameHr\Application\User\Create\UserCreateRequest;
use App\SesameHr\Application\User\Create\UserCreateUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Event\EventBus;
use App\SesameHr\Shared\Domain\Exception\BadRequestException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UsersMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class UserCreateUseCaseTest extends TestCase
{
    private UserRepository $userRepository;
    private EventBus $eventBus;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
        $this->eventBus = $this->createMock(EventBus::class);
    }

    #[Test]
    public function shouldCreateNewUser(): void
    {
        $this->userRepository
            ->method('search')
            ->willReturn(UsersMother::of());
        $this->userRepository
            ->expects($this->once())
            ->method('save');
        $this->eventBus
            ->expects($this->once())
            ->method('publish');
        $user = UserMother::random();
        $request = new UserCreateRequest($user->name(), (string)$user->email());

        $response = (new UserCreateUseCase($this->userRepository, $this->eventBus))($request);

        $this->assertArrayHasKey('id', $response->data());
        $this->assertEquals($user->name(), $response->data()['name']);
        $this->assertEquals((string)$user->email(), $response->data()['email']);
        $this->assertArrayHasKey('createdAt', $response->data());
        $this->assertArrayHasKey('updatedAt', $response->data());
    }

    #[Test]
    public function shouldThrowErrorWhenUserExists(): void
    {
        $this->userRepository
            ->method('search')
            ->willReturn(UsersMother::random());
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $this->eventBus
            ->expects($this->never())
            ->method('publish');
        $request = new UserCreateRequest(StringMother::name(), StringMother::email());

        $this->expectException(BadRequestException::class);

        (new UserCreateUseCase($this->userRepository, $this->eventBus))($request);
    }
}
