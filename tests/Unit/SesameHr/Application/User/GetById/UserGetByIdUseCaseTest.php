<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Application\User\GetById;

use App\SesameHr\Application\User\GetById\UserGetByIdRequest;
use App\SesameHr\Application\User\GetById\UserGetByIdUseCase;
use App\SesameHr\Domain\User\UserRepository;
use App\SesameHr\Shared\Domain\Exception\NotFoundException;
use App\Tests\Mother\StringMother;
use App\Tests\Unit\SesameHr\Domain\User\Mother\UserMother;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class UserGetByIdUseCaseTest extends TestCase
{
    private UserRepository $userRepository;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepository = $this->createMock(UserRepository::class);
    }

    #[Test]
    public function shouldGetUserById(): void
    {
        $user = UserMother::random();
        $this->userRepository
            ->method('ofId')
            ->willReturn($user);
        $request = new UserGetByIdRequest((string)$user->id());

        $response = (new UserGetByIdUseCase($this->userRepository))($request);

        $this->assertEquals((string)$user->id(), $response->data()['id']);
        $this->assertEquals($user->name(), $response->data()['name']);
        $this->assertEquals((string)$user->email(), $response->data()['email']);
        $this->assertEquals((string)$user->createdAt(), $response->data()['createdAt']);
        $this->assertEquals((string)$user->updatedAt(), $response->data()['updatedAt']);
    }

    #[Test]
    public function shouldThrowErrorWhenUserNotExists(): void
    {
        $this->userRepository
            ->method('ofId')
            ->willReturn(null);
        $this->userRepository
            ->expects($this->never())
            ->method('save');
        $request = new UserGetByIdRequest(StringMother::uuid());

        $this->expectException(NotFoundException::class);

        (new UserGetByIdUseCase($this->userRepository))($request);
    }
}
