<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Shared\Domain;

use App\SesameHr\Shared\Domain\Exception\InternalServerErrorException;
use App\SesameHr\Shared\Domain\Uuid;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

class UuidTest extends TestCase
{
    #[Test]
    public function shouldCreateEmail(): void
    {
        $validValue = 'c43cb105-afdd-4670-8d25-8ec0c548d9ec';

        $email = Uuid::from($validValue);

        $this->assertEquals($validValue, (string)$email);
    }

    #[Test]
    public function shouldThrowExceptionWhenUuidIsInvalid(): void
    {
        $invalidValue = 'c43cb105-afdd';

        $this->expectException(InternalServerErrorException::class);

        Uuid::from($invalidValue);
    }
}
