<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Shared\Domain\Mother;

use App\SesameHr\Shared\Domain\Email;
use App\Tests\Mother\StringMother;

final class EmailMother
{
    public static function random(): Email
    {
        return Email::from(StringMother::email());
    }
}
