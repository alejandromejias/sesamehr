<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Shared\Domain\Mother;

use App\SesameHr\Shared\Domain\Uuid;
use App\Tests\Mother\StringMother;

final class UuidMother
{
    public static function random(): Uuid
    {
        return Uuid::from(StringMother::uuid());
    }
}
