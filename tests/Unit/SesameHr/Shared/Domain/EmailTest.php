<?php

declare(strict_types=1);

namespace App\Tests\Unit\SesameHr\Shared\Domain;

use App\SesameHr\Shared\Domain\Email;
use App\SesameHr\Shared\Domain\Exception\InternalServerErrorException;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\TestCase;

final class EmailTest extends TestCase
{
    #[Test]
    public function shouldCreateEmail(): void
    {
        $validValue = 'johndoe@domain.com';

        $email = Email::from($validValue);

        $this->assertEquals($validValue, (string)$email);
    }

    #[Test]
    public function shouldThrowExceptionWhenEmailIsInvalid(): void
    {
        $invalidValue = 'johndoe@';

        $this->expectException(InternalServerErrorException::class);

        Email::from($invalidValue);
    }
}
